
Installation:
Log into the Drupal 7 as Administrator.
Go to "admin/modules".
Select the module with name "Accessiblizer".
Click "Save Configuration".

Configuration: 
Accessiblizer settings form  at "admin/config/system/accessiblizer"

NOTE
Replace "js/accessiblizer_dummy.js" with your script file from "http://www.accessiblizer.com"